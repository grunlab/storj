#!/bin/bash

if [ -z "$(ls -A /app/config)" ]; then
  SETUP="true"
  /entrypoint
  sed 's/operator.wallet-features: ""/operator.wallet-features: ["zksync-era"]/g' -i /app/config/config.yaml
fi

SETUP="false"
/entrypoint