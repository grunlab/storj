[![pipeline status](https://gitlab.com/grunlab/storj/badges/main/pipeline.svg)](https://gitlab.com/grunlab/storj/-/commits/main)

# GrunLab Storj

Storj non-root container images (storagenode with storj-earnings embedded, multinode) and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/storj.md
- https://docs.grunlab.net/apps/storj.md

Base images:
- [docker.io/storjlabs/multinode:latest][multinode]
- [docker.io/storjlabs/storagenode:latest][storagenode]

Format: docker

Supported architecture(s):
- arm64

GrunLab project(s) using this service:
- [grunlab/storj][storj]

[multinode]: <https://hub.docker.com/r/storjlabs/multinode>
[storagenode]: <https://hub.docker.com/r/storjlabs/storagenode>
[storj]: <https://gitlab.com/grunlab/storj>